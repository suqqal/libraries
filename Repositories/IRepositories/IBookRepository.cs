﻿using Labraries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Libraries.Repositories.IRepositories {
    public interface IBookRepository {
        public Book GetOne(string BookId, string LibId);
        public Book Add(Book NewBook);
        public Book Update(string BookId, Book NewData);
        public Book Delete(string Id, string LibId);
    }
}
