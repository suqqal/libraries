﻿using Labraries.Models;
using Labraries.Persistence;
using Libraries.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Libraries.Repositories {
    public class BookRepository : IBookRepository {
        private readonly LibrariesDBContext context;

        public BookRepository(LibrariesDBContext context) {
            this.context = context;
        }
        

        Book IBookRepository.Add(Book NewBook) {
            context.Books.Add(NewBook);
            context.SaveChanges();
            return NewBook;
        }

        Book IBookRepository.Delete(string Id, string LibId) {
            var book = context.Books.Where(p => p.Id.Equals(Id) &&
                                            p.LabraryId.Equals(LibId))
                                .FirstOrDefault();
            if (book == null)
                return null;
            context.Books.Remove(book);
            context.SaveChanges();
            return book;
        }

        Book IBookRepository.GetOne(string BookId, string LibId) {
            return context.Books.Where(p=>p.Id.Equals(BookId) &&
                                          p.LabraryId.Equals(LibId))
                                .FirstOrDefault();
        }

        Book IBookRepository.Update(string BookId, Book NewData) {
            var book=context.Books.Where(p => p.Id.Equals(BookId) &&
                                          p.LabraryId.Equals(NewData.LabraryId))
                                .FirstOrDefault();
            if (book == null)
                return null;
            book.Name = NewData.Name;
            book.PublishedAt = NewData.PublishedAt;
            book.Author = NewData.Author;
            context.SaveChanges();
            return book;
        }
    }
}
