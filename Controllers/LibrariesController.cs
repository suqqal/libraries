﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Labraries.Models;
using Labraries.Persistence;
using AutoMapper;
using Labraries.Repositories.IRepositories;
using Labraries.Controllers.DTO;
using Libraries.Controllers.DTO;

namespace Labraries.Controllers {
    [Route("libraries")]
    public class LibrariesController : Controller {
        private readonly IMapper mapper;
        private readonly ILibraryRepository libraryRepository;

        public LibrariesController(IMapper mapper, ILibraryRepository libraryRepository) {
            this.mapper = mapper;
            this.libraryRepository = libraryRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetLabraries() {
            var Libraries = libraryRepository.GetAll();
            return Ok(mapper.Map<ICollection<LibraryDTO>>(Libraries));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id) {
            var library = libraryRepository.GetOne(id);
            if (library == null)
                return NotFound();
            var LibDto = mapper.Map<Library, LibraryDTO>(library);
            LibDto.Books = mapper.Map<ICollection<BookDTO>>(library.Books);
            return Ok(LibDto);
        }

        [HttpPost]
        public IActionResult Add([FromBody]UpsertLibraryDTO NewLib) {
            if (NewLib == null)
                return BadRequest();
            var Library = libraryRepository.Add(mapper.Map<UpsertLibraryDTO, Library>(NewLib));
            return Ok(mapper.Map<Library, LibraryDTO>(Library));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(string id,[FromBody] UpsertLibraryDTO NewData) {
            if (NewData == null)
                return BadRequest();
            var library = libraryRepository.Update(id,mapper.Map<UpsertLibraryDTO, Library>(NewData));
            if (library == null) 
                return NotFound();
            return Ok(library);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id) {
            var library = libraryRepository.Delete(id);
            if (library == null) 
                return NotFound();
            return NoContent();
        }


    }
}
