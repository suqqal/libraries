﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Labraries.Migrations
{
    public partial class Seeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "labrary",
                columns: new[] { "Id", "Address", "Name", "Phone" },
                values: new object[] { "1", "Midan", "First labrary", "+9999999999" });

            migrationBuilder.InsertData(
                table: "labrary",
                columns: new[] { "Id", "Address", "Name", "Phone" },
                values: new object[] { "2", "Hamra street", "Second labrary", "+9999999998" });

            migrationBuilder.InsertData(
                table: "labrary",
                columns: new[] { "Id", "Address", "Name", "Phone" },
                values: new object[] { "3", "Somewhere", "Third labrary", "+9999999997" });

            migrationBuilder.InsertData(
                table: "book",
                columns: new[] { "Id", "Author", "LabraryId", "Name", "PublishedAt" },
                values: new object[,]
                {
                    { "1", "Author 1", "1", "First Book", new DateTime(2000, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "2", "Author 2", "1", "Seconde Book", new DateTime(1958, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "3", "Author 1", "2", "Third Book", new DateTime(2003, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { "4", "Author 4", "2", "Fourth Book", new DateTime(1999, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "book",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DeleteData(
                table: "book",
                keyColumn: "Id",
                keyValue: "2");

            migrationBuilder.DeleteData(
                table: "book",
                keyColumn: "Id",
                keyValue: "3");

            migrationBuilder.DeleteData(
                table: "book",
                keyColumn: "Id",
                keyValue: "4");

            migrationBuilder.DeleteData(
                table: "labrary",
                keyColumn: "Id",
                keyValue: "3");

            migrationBuilder.DeleteData(
                table: "labrary",
                keyColumn: "Id",
                keyValue: "1");

            migrationBuilder.DeleteData(
                table: "labrary",
                keyColumn: "Id",
                keyValue: "2");
        }
    }
}
