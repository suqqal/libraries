﻿using AutoMapper;
using Labraries.Controllers.DTO;
using Labraries.Models;
using Libraries.Controllers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Mapper {
    public class MappingProfile : Profile {
        public MappingProfile() {
            CreateMap<Library, LibraryDTO>()
              .ForMember(v => v.BooksCount, Opt => Opt.MapFrom(u => u.Books.Count));
            
            CreateMap<Book, BookDTO>();

            CreateMap<UpsertLibraryDTO, Library>();
            
            CreateMap<UpsertBookDTO, Book>();

        }
    }
}
