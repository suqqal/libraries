﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Libraries.Helpers {
    public class FilesHelper {
        public string rootpath { get; set; }

        public FilesHelper(IWebHostEnvironment host) {
            this.rootpath = host.ContentRootPath;
        }
        
        public string createDirectory(string path) {
            var dir = this.rootpath;
            if (path != null)
                dir = Path.Combine(this.rootpath, $"{path}/");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return dir;
        }

        public void Write(string Message, string path,string FileName) {
            var dir = createDirectory(path);
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(dir, FileName), true)) {
                outputFile.WriteLine(Message);
            }
        }
    }
}
