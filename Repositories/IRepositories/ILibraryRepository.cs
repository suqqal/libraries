﻿using Labraries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Repositories.IRepositories {
    public interface ILibraryRepository {
        public ICollection<Library> GetAll();
        public Library GetOne(string Id);
        public Library Add(Library NewLabrary);
        public Library Update(string Id,Library NewData);
        public Library Delete(string Id);
    }
}
