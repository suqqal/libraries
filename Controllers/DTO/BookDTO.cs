﻿
using System;

namespace Labraries.Controllers.DTO {
    public class BookDTO {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public DateTime? PublishedAt { get; set; }
    }
}