﻿using Labraries.Models;
using Labraries.Persistence;
using Labraries.Repositories.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Repositories {
    public class LibraryRepository : ILibraryRepository {
        private readonly LibrariesDBContext context;

        public LibraryRepository(LibrariesDBContext context) {
            this.context = context;
        }

        public Library Add(Library NewLabrary) {
            context.Libraries.Add(NewLabrary);
            context.SaveChanges();
            return NewLabrary;
        }

        public Library Delete(string Id) {
            var Lib = context.Libraries.Find(Id);
            if (Lib == null)
                return null;
            context.Libraries.Remove(Lib);
            context.SaveChanges();
            return Lib;
        }

        public ICollection<Library> GetAll() {
            return context.Libraries.Include(p => p.Books)
                                             .ToList();
        }

        public Library GetOne(string Id) {
            return context.Libraries.Where(p => p.Id.Equals(Id))
                                    .Include(b => b.Books)
                                    .FirstOrDefault();
        }

        public Library Update(string Id, Library NewData) {
            var Lib = context.Libraries.Find(Id);
            if (Lib == null)
                return null;
            Lib.Name = NewData.Name;
            Lib.Phone = NewData.Phone;
            Lib.Address = NewData.Address;
            context.SaveChanges();
            return Lib;
        }
    }
}
