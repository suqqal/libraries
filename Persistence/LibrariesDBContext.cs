﻿using Labraries.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Persistence {
    public class LibrariesDBContext : DbContext {

        public DbSet<Library> Libraries{ get; set; }
        public DbSet<Book> Books{ get; set; }
        public LibrariesDBContext(DbContextOptions<LibrariesDBContext> options)
            : base(options) {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Book>().HasIndex(b => b.Name).IsUnique();
            modelBuilder.Entity<Library>().HasMany(p => p.Books).WithOne(p => p.Labrary).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.seed();

        }
    }
}
