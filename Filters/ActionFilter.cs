﻿using Libraries.Helpers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Libraries.Filter {
    public class ActionFilter : IActionFilter,IExceptionFilter {
        private FilesHelper filesHelper;
        public ActionFilter(FilesHelper filesHelper) {
            this.filesHelper = filesHelper;

        }
        public void OnActionExecuted(ActionExecutedContext context) {
            return ;
        }

        public void OnActionExecuting(ActionExecutingContext context) {
            filesHelper.Write($"{DateTime.Now}:{context.ActionDescriptor.DisplayName}", "Log","Log.txt");
        }

        public void OnException(ExceptionContext context) {
            filesHelper.Write($"{DateTime.Now}:{context.Exception.StackTrace}", "Log", "Exceptions.txt");
        }
    }
}
