﻿using AutoMapper;
using Labraries.Controllers.DTO;
using Labraries.Models;
using Libraries.Controllers.DTO;
using Libraries.Repositories.IRepositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Libraries.Controllers {
    [Route("libraries/{libId}/books")]
    public class BookController : Controller {
        public readonly IBookRepository bookRepository;
        public IMapper mapper;

        public BookController(IBookRepository bookRepository,IMapper mapper) {
            this.bookRepository = bookRepository;
                this.mapper = mapper;
        }

        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetBook(string bookId,string libId) {
            var book = bookRepository.GetOne(bookId, libId);
            if (book == null)
                return NotFound();
            return Ok(mapper.Map<Book,BookDTO>(book));
        }
        
        [HttpPost]
        public IActionResult Add(string libId,[FromBody]UpsertBookDTO NewBook) {
            if (NewBook == null)
                return null;
            var book = mapper.Map<UpsertBookDTO, Book>(NewBook);
            book.LabraryId = libId;
            book = bookRepository.Add(book);
            return Ok(book);
        }

        [HttpPut("{bookId}")]
        public async Task<IActionResult> Edit(string libId,string bookId, [FromBody] UpsertBookDTO NewData) {
            if (NewData == null)
                return BadRequest();
            var book = mapper.Map<UpsertBookDTO, Book>(NewData);
            book.LabraryId = libId;
            book = bookRepository.Update(bookId, book);
            if (book == null)
                return NotFound();
            return Ok(book);
        }

        [HttpDelete("{bookId}")]
        public async Task<IActionResult> Delete(string libId,string bookId) {
            var book = bookRepository.Delete(bookId,libId);
            if (book == null)
                return NotFound();
            return NoContent();
        }
    }
}
