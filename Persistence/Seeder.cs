﻿using Labraries.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Persistence {
    public static class Seeder {
        public static void seed(this ModelBuilder modelBuilder) {
            SeedLabraries(modelBuilder);
            SeedBooks(modelBuilder);

        }

        private static void SeedLabraries(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Library>().HasData(
                    new Library {
                        Id = "1",
                        Name = "First labrary",
                        Phone= "+9999999999",
                        Address = "Midan"
                    },
                    new Library {
                        Id = "2",
                        Name = "Second labrary",
                        Phone = "+9999999998",
                        Address = "Hamra street"
                    },
                    new Library {
                        Id = "3",
                        Name = "Third labrary",
                        Phone = "+9999999997",
                        Address = "Somewhere"
                    }
                   );
        }
        private static void SeedBooks(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Book>().HasData(
                    new Book {
                        Id = "1",
                        Name = "First Book",
                        Author = "Author 1",
                        LabraryId = "1",
                        PublishedAt = DateTime.Parse("5/5/2000")
                    },
                    new Book {
                        Id = "2",
                        Name = "Seconde Book",
                        Author = "Author 2",
                        LabraryId = "1",
                        PublishedAt = DateTime.Parse("5/5/1958")
                    },
                   new Book {
                       Id = "3",
                       Name = "Third Book",
                       Author = "Author 1",
                       LabraryId = "2",
                       PublishedAt = DateTime.Parse("1/1/2003")
                   },
                   new Book {
                       Id = "4",
                       Name = "Fourth Book",
                       Author = "Author 4",
                       LabraryId = "2",
                       PublishedAt = DateTime.Parse("12/12/1999")
                   }
                   );
        }

    }
}
