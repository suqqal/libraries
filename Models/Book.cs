﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Models {
    [Table("book")]
    public class Book {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LabraryId{ get; set; }
        public string Author{ get; set; }
        public DateTime? PublishedAt{ get; set; }
        public Library Labrary{ get; set; }

    }
}
