﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Labraries.Controllers.DTO {
    public class LibraryDTO {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int BooksCount { get; set; }
        public ICollection<BookDTO> Books { get; set; }

        public LibraryDTO() {
            Books = new Collection<BookDTO>();
        }
    }
}
